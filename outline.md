
# Introduction

## Problème: quelles sont les dépendances externes d'un fichier de code source ?

Trivial dans le lambda-calcul: variables libres du terme.
Souvent forcé explicitement par le langage: "import" de Haskell ou de Python.
Ou fixé par des conventions telles le include "header.h" de C ou C++.

Peut-on le faire implicitement comme dans le lambda-calcul ?
  => dépend de la richesse du langage de scoping, "système de modules"

Dans notre cas d'étude: OCaml, langage avec système de module riche, mais dépendances calculées implicitement (quand ça marche).
<exemples *simples* de feature de modules qui rendent la question non-triviale: open, include, sous-modules>
(non, pas de modules de première classe ici)

On explique une méthode robuste pour le faire, plus robuste que l'approximation existante, et les cas où ça ne
marche pas.


### Questions d'ingénérie d'une implémentation d'un langage

En théorie c'est facile à faire à partir de l'AST typé, mais:
- Le typeur peut être lent¹ ou fragile/compliqué.
- L'œuf ou la poule ? Le typeur a besoin que les dépendances externes soient résolues pour typer les parties du programme qui les utilisent.
- Il est pratique de pouvoir calculer des dépendances pour des sources qui ne sont pas bien typées car en cours d'écriture.²

Intégrer la résolution des dépendances au typage complique encore le typeur.
on veut découpler les deux aspects et faire la dépendance de la façon la plus simple (et rapide) possible, en
amont.

¹: la lenteur de la résolution des dépendances est un bottleneck pour les outils de build parallèle, donc ce n'est pas à négliger.
²: Merlin sait auto-compléter un buffer partiel en un AST raisonnable, mais pas en un AST bien typé. On peut donc raisonnablement supposer qu'on a un AST complet à tout moment, mais pas qu'il est typable.


## Grandes lignes de la contribution

Idée: une analyse simplifiée qui calcule un-e "outline": la forme des modules des unités de compilation.
=> l'information minimale qui suffit à résoudre les dépendances

Analyseur qui résoud les dépendances au vol grâce à une forme d'interruptibilité.

Gère bien la plupart des features des modules OCaml (en particulier: module aliases), contrairement à l'approximation précédente ocamldep (TODO: l'expliquer avant, ou pas envie de le faire en introduction ?) qui demande des flags supplémentaires pour les gérer.

Warnings là où une perte de précision est inévitable. (Pointer vers une section future avec des exemples.)

Code pas trop long, performances pas trop mauvaises: c'est utilisable en pratique.


## Nos contributions
paragraphe très important:
    "We claim the following contributions:
     1. ...
     2. ...
     3. ..."



# Sections de contenu

## Section théorique, langage jouet

Idée de formalisation (à creuser):
    - avoir un langage module jouet avec "module =", include et open
        (+ aliases ?)
        (Remarque: du coup les outlines sont très proches de la signature. Gabriel trouve ça normal.)
    - définir un jugement de scoping pour résoudre les noms
      (dans un environnement externe fourni au jugement)
    - formaliser un analyseur d'outlines comme un interprète "statique"
      (interprète abstrait ? éviter de donner des espoirs de liens trop forts avec les aspects avancés de l'interprétation abstraite)
    - prouver quoi sur cet analyseur d'outlines ?
      + terminaison
      + que l'interpréteur identifie les modules qui ne font pas partie de l'environnement

Que pourrait-on prouver en présence de modules non résolus ?
=> on calcule une sur-approximation des modules libres
On peut montrer:
    - quand tous les identifiants de modules sont connus, on se retrouve avec le résultat exact
    - mais sinon on peut quand même calculer une sur-approximation valide

Codept fonctionne en mode "approximatif" sous un module inconnu.
Il est important/intéressant d'inclure cela dans notre modèle simplifié.

Par exemple lorsque qu'on a des alias, accéder un module après l'ouverture d'un module inconnu peut ajouter plusieurs dépendances simultanément.

(Ajouter les features plus compliquées graduellement, en particulier les alias dans un second temps ?)

Note: le langage jouet n'est pas si différent et important des fonctionnalités vraiment importantes du langage.
Cf. section "limitations".


## Background: ocamldep

Point important: parfois ocamldep échoue à sur-approximer correctement.
Support douloureux (et explicite) des module aliases.


## L'implémentation de codept est jolie

(détailler la structure d'ensemble de haut niveau)

(éventuellement c'est le moment de mentionner la double sortie JSON/sexp:
avec un GADT de représentation dynamique de type odnt on sort aussi un schéma JSON)


### [Le zipper | La monade] [ séquentiel-le | parallèle ] de la mort.

=> découplage du code de traversée de l'AST et de résolution des dépendances


### Stratégies de résolution multi-modules


### Messages d'erreur


### Métriques à la con (ligne de code, performance, etc.)

(pas de section sur l'adoption parce que pas d'adoption)


## Limitations liées aux features bizarres (et rarement utilisées) de OCaml

Important: codept sait faire un Warning dans ces cas, donc on sait qu'on est en mode dégradé.
Important: la dégradation est toujours une sur-approximation, pas une sous-approximation
 (problème d'ocamldep)


### Modules de premièree classe à la signature implicite


### Polymorphisme de signature dans les foncteurs



# Conclusion

## Related Work
  (très important!)

TODO biblio:
  - faire un tour des langages fonctionnels pour voir ce qu'ils utilisent
    (et/ou demander à Frédéric)
  - chercher en ligne
  - aller voir l'article sur le système de build optimal, il pourrait y avoir des pointeurs
  - si on sèche, demander à types-list


## Future Work
   (seulement si on a un truc à dire)




0# Dump:


Prochaine réunion: mercredi prochain.

TODO pour la prochaine fois
- Florian: créer un dépôt git et pousser notre brouillon
- Florian: avancer sur la bibliographie
  (Gabriel: envoyer pointeurs si trouvés)
- Gabriel: transformer le plan détaillé en un outline TeX qui va bien

TODO en synchrone:
- travailler sur la partie formalisation (plutôt tôt)

Pour les "implémentations jolies de la mort" on attend un peu, Florian est en train de changer codept pour 4.10 et c'est mieux de le faire quand ça sera re-stabilisé.


